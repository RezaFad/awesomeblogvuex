FROM node:lts-alpine as build-stage
RUN mkdir /app
ADD . /app
WORKDIR /app
COPY package*.json ./
RUN npm install
RUN npm install -g @quasar/cli
COPY . .
RUN quasar dev

#prodcution stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]