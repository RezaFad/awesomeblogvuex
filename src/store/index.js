import Vue from 'vue'
import Vuex from 'vuex'

import blogs from './showcase/index'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      blogs
    },
    strict: process.env.DEV
  })

  if (process.env.DEV && module.hot) {
    module.hot.accept(['./showcase'], () => {
      const newShowcase = require('./showcase').default
      Store.hotUpdate({ modules: { showcase: newShowcase } })
    })
  }

  return Store
}
