export default function () {
  return {
    blogs: [
      {
        id: 'ID1',
        title: 'Be Super Hero in Avengers with code + Tips',
        content: 'Everyone loves cheese and wine bavarian bergkase.Gouda cheese and wine swiss taleggio port-salut cheese slices boursin gouda. Paneer lancashire port-salut pecorino melted cheese cheesy feet caerphilly stilton. Boursin st. agur blue cheese red leicester red leicester taleggio red leicester cheese triangles cheddar. Squirty cheese chalk and cheese pepper jack when the cheese comes out everybodys happy. Lancashire everyone loves stinking bishop. Camembert de normandie pepper jack cheese triangles brie dolcelatte chalk and cheese babybel fromage. Jarlsberg say cheese feta babybel melted cheese parmesan squirty cheese brie. Pecorino lancashire cheese slices jarlsberg cheese and wine bavarian bergkase. Paneer manchego bavarian bergkase. Gouda cheesecake blue castello cottage cheese chalk and cheese squirty cheese caerphilly emmental. Gouda edam cauliflower cheese squirty cheese stilton fromage macaroni cheese pecorino. Cottage cheese. Croque monsieur cream cheese fromage frais. Cheese triangles airedale melted cheese dolcelatte cheese on toast cut the cheese pepper jack croque monsieur. Swiss when the cheese comes out everybodys happy swiss cheese on toast monterey jack ricotta gouda stinking bishop. Cheeseburger airedale cheesecake cheese slices airedale croque monsieur taleggio. Paneer camembert de normandie fondue. Emmental cheesecake cauliflower cheese gouda swiss macaroni cheese say cheese brie. Cottage cheese pepper jack danish fontina jarlsberg pepper jack cheesy feet swiss cheesy feet. Swiss.',
        publishDate: '2020/08/21',
        publishTime: '08.30',
        authors: 'Jenny Robert',
        avatars: 'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80',
        Categories: ['Technology', 'Science']
      },
      {
        id: 'ID2',
        title: 'How to build colony in Mars',
        content: 'Cheese triangles cheese slices st. agur blue cheese. Jarlsberg fromage cheddar st. agur blue cheese hard cheese cheeseburger croque monsieur cheeseburger. Port-salut mascarpone manchego cream cheese squirty cheese cheeseburger taleggio macaroni cheese. Halloumi camembert de normandie who moved my cheese fromage frais bocconcini cheesy grin cheesecake parmesan. Rubber cheese cottage cheese the big cheese.',
        publishDate: '2020/11/1',
        publishTime: '09.10',
        authors: 'Elon Mask',
        avatars: 'https://www.incimages.com/uploaded_files/image/1920x1080/getty_489769706_200013332000928069_437741.jpg',
        Categories: ['Lifestyle', 'Science']
      },
      {
        id: 'ID3',
        title: 'Using MacBook Pro 2020 For Coding',
        content: 'Cheese triangles cheese slices st. agur blue cheese. Jarlsberg fromage cheddar st. agur blue cheese hard cheese cheeseburger croque monsieur cheeseburger. Port-salut mascarpone manchego cream cheese squirty cheese cheeseburger taleggio macaroni cheese. Halloumi camembert de normandie who moved my cheese fromage frais bocconcini cheesy grin cheesecake parmesan. Rubber cheese cottage cheese the big cheese.',
        publishDate: '2020/09/1',
        publishTime: '10.12',
        authors: 'Steve Jobs',
        avatars: 'https://cdn.vox-cdn.com/thumbor/gD8CFUq4EEdI8ux04KyGMmuIgcA=/0x86:706x557/920x613/filters:focal(0x86:706x557):format(webp)/cdn.vox-cdn.com/imported_assets/847184/stevejobs.png',
        Categories: ['Software']
      },
      {
        id: 'ID4',
        title: 'Facebook launch the new features for Developer',
        content: 'Cheese triangles cheese slices st. agur blue cheese. Jarlsberg fromage cheddar st. agur blue cheese hard cheese cheeseburger croque monsieur cheeseburger. Port-salut mascarpone manchego cream cheese squirty cheese cheeseburger taleggio macaroni cheese. Halloumi camembert de normandie who moved my cheese fromage frais bocconcini cheesy grin cheesecake parmesan. Rubber cheese cottage cheese the big cheese.',
        publishDate: '2020/09/12',
        publishTime: '12.12',
        authors: 'Mark',
        avatars: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg/1200px-Mark_Zuckerberg_F8_2019_Keynote_%2832830578717%29_%28cropped%29.jpg',
        Categories: ['Technology']
      },
      {
        id: 'ID5',
        title: 'How To Make Italian Pizza (Homemade)',
        content: 'Cheese triangles cheese slices st. agur blue cheese. Jarlsberg fromage cheddar st. agur blue cheese hard cheese cheeseburger croque monsieur cheeseburger. Port-salut mascarpone manchego cream cheese squirty cheese cheeseburger taleggio macaroni cheese. Halloumi camembert de normandie who moved my cheese fromage frais bocconcini cheesy grin cheesecake parmesan. Rubber cheese cottage cheese the big cheese.',
        publishDate: '2020/09/15',
        publishTime: '10.12',
        authors: 'Tony Stark',
        avatars: 'https://img.cinemablend.com/filter:scale/quill/d/9/5/a/c/9/d95ac9d6f0acd56e12b9452301c763cd1aeffe51.jpg?fw=1200',
        Categories: ['Software']
      },
      {
        id: 'ID6',
        title: 'Today! Apple Launch new iPone 12 and iPad 2021',
        content: 'Cheese triangles cheese slices st. agur blue cheese. Jarlsberg fromage cheddar st. agur blue cheese hard cheese cheeseburger croque monsieur cheeseburger. Port-salut mascarpone manchego cream cheese squirty cheese cheeseburger taleggio macaroni cheese. Halloumi camembert de normandie who moved my cheese fromage frais bocconcini cheesy grin cheesecake parmesan. Rubber cheese cottage cheese the big cheese.',
        publishDate: '2020/09/19',
        publishTime: '10.12',
        authors: 'Russhel',
        avatars: 'https://media.hitekno.com/thumbs/2020/06/22/59739-apple-wwdc-2020/730x480-img-59739-apple-wwdc-2020.jpg',
        Categories: ['Software']
      }
    ],
    users: [
      {
        id: 'A1',
        name: 'Jhon Doe',
        username: 'jhondoe',
        password: '12345'
      },
      {
        id: 'A2',
        name: 'Steve',
        username: 'steve',
        password: '12345'
      }
    ]
  }
}
