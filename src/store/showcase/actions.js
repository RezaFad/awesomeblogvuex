export function addBlogs ({ commit }, payload) {
  commit('addBlogsMut', payload)
}

export function updateBlogs ({ commit }, payload) {
  commit('updateBlogsMut', payload)
}

export function addUsers ({ commit }, payload) {
  commit('addUsersMut', payload)
}
